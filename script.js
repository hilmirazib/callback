class Table {
    constructor(init) {
        this.init = init;
    }

    createHeader(data) {
        let open = '<thead><tr>';
        let close = '</tr></thead>';
        data.forEach((d) => {
            open += `<th>${d}</th>`;
        });

        return open + close;
    }

    createBody(data) {
        fetch(data)
            .then((response) => response.json())
            .then((res) => {
                let open = '';
                let close = '</tbody>';
                res.forEach((d) => {
                    open += `
        <tr>
          <td>${d.id}</td>
          <td>${d.name}</td>
          <td>${d.username}</td>
          <td>${d.email}</td>
          <td>${d.address.street},${d.address.suite}, ${d.address.city}</td>
          <td>${d.company.name}</td>
        </tr>
      `;
                    console.log(d.id);
                });

                document.getElementById('unik').innerHTML = open + close;
            });
    }

    render(element) {
        let table = "<table class='table table-hover table-striped table-dark'>" + this.createHeader(this.init.columns) + "<tbody id='unik'>";
        this.createBody(this.init.url) + '</table>';
        element.innerHTML = table;
    }
}
const table = new Table({
    columns: ['ID', 'Name', 'Username', 'Email', 'Adress', 'Company'],

    url: 'https://jsonplaceholder.typicode.com/users',
});

const wrap = document.getElementById('wrapper');
table.render(wrap);